package com.ibagroup.shuttleninja.web.rest;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import com.ibagroup.shuttleninja.database.dao.SeatSearchParams;
import com.ibagroup.shuttleninja.database.model.Seat;
import com.ibagroup.shuttleninja.service.SeatService;

@Path("seat")
@Stateless
public class SeatEndpoint {

	/**
	 * <p>{@link BookingService} injection</p>
	 */
	@Inject
	private SeatService service;
	
	/**
	 * 
	 * <p>Method for retrieve all reservations without any search criteria.</p>
	 * 
	 * @return a list of all reservations
	 */
    @GET
    @Produces("application/json")
    public List<Seat> retriveAllSeats() {
    	try {
    		return service.retrieveAllSeats();
    	} catch (Exception e) {
			return null;
		}
    }
    
	/**
	 * 
	 * <p>Method for retrieve all reservations without any search criteria.</p>
	 * 
	 * @return a list of all reservations
	 */
    @GET
    @Path("/search")
    @Produces("application/json")
    public List<Seat> retriveSeatsByParams() {
    	try {
    		SeatSearchParams params = new SeatSearchParams();
    		return service.retrieveSeatByParams(params);
    	} catch (Exception e) {
			return null;
		}
    }
    
    /**
	 * 
	 * <p>Method for retrieve booking by ID.</p>
	 * 
	 * @return a reservation by ID
	 */
    @GET
    @Path("/search/{id:[0-9][0-9]*}")
    @Produces("application/json")
    public List<Seat> retriveSeatById(@PathParam("id") Long id) {
    	try {
	    		SeatSearchParams params = new SeatSearchParams();
	    		params.setId(id);
	    		return service.retrieveSeatByParams(params);
    	} catch (Exception e) {
				return null;
		}
    }
    
    /**
	  * <p>Method for retrieve employee by surname.</p>
	  * 
	  * @param bean {@link EmployeeBean}
	  * @throws Exception
	  */
	@POST
	@Path("/search/params")
	@Produces("application/json")
	public List<Seat> retrieveBookingByTimeParams(SeatSearchParams params) throws Exception {
		try {
	  			return service.retrieveSeatByParams(params);
	    	} catch (Exception e) {
	    		return null;
			}
	}
    
//    /**
//	 * 
//	 * <p>Method for retrieve reservation by employee ID.</p>
//	 * 
//	 * @return a reservation by employee ID
//	 */
//    @GET
//    @Path("/employee/{id:[0-9][0-9]*}")
//    @Produces("application/json")
//    public List<ReservationBean> retrieveREservationByEmployeeId(@PathParam("id") Long id) {
//    	try {
//    		ReservationSearchParams params = new ReservationSearchParams();
//    		params.setReservedBy(id);
//    		return mapperToBean.toBeanList(service.retrieveReservationByEmployeeId(params));
//    	} catch (Exception e) {
//			return null;
//		}
//    }
    
    /**
     * 
     * <p>Method for delete booking by ID</p>
     * 
     * @param id
     */
    @DELETE
    @Path("/{id:[0-9][0-9]*}")
    public void deleteSeatById(@PathParam("id") Long id) {
    	try {
    			service.deleteSeatById(id);
    	} catch (Exception e) {
    	
		}
    }
    
    /**
     * 
     * <p>Method for create new booking.</p>
     * 
     * @param dto
     * @throws Exception
     */
    @POST
    @Consumes("application/json")
    public Response createNewSeat(Seat dto) throws Exception {
    	try {
    			service.createNewSeat(dto);
    	} catch (Exception e) {
    	
		}
		return Response.created(UriBuilder.fromResource(SeatEndpoint.class).path(String.valueOf(dto.getId())).build()).build();
    };
	
//    /**
//     * 
//     * <p>Method for update reservation by their id.</p>
//     * 
//     * @param id
//     * @param bean {@link ReservationBean}
//     */
//    @PUT
//    @Path("/{id:[0-9][0-9]*}")
//    @Consumes("application/json")
//    public void updateReservationById(@PathParam("id") Long id, ReservationBean bean) {
//    	try {
//    		service.updateReservationById(id, mapperToDto.toDto(bean));
//    	} catch (Exception e) {
//    		
//		}
//    }
	
}
