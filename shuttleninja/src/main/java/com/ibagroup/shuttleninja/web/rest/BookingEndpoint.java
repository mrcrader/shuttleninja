package com.ibagroup.shuttleninja.web.rest;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import com.ibagroup.shuttleninja.database.dao.BookingSearchParams;
import com.ibagroup.shuttleninja.database.model.Booking;
import com.ibagroup.shuttleninja.service.BookingService;
import com.ibagroup.shuttleninja.util.CurrentTimeInTimetamp;

@Path("booking")
@Stateless
public class BookingEndpoint {

	/**
	 * <p>{@link BookingService} injection</p>
	 */
	@Inject
	private BookingService service;
	
	@Inject
	private CurrentTimeInTimetamp currentTime;
    
	/**
	 * 
	 * <p>Method for retrieve all reservations without any search criteria.</p>
	 * 
	 * @return a list of all reservations
	 */
    @GET
    @Produces("application/json")
    public List<Booking> retriveAllBooking() {
    	try {
    		BookingSearchParams params = new BookingSearchParams();
    		return service.retrieveBooking(params);
    	} catch (Exception e) {
			return null;
		}
    }
    
    /**
	 * 
	 * <p>Method for retrieve booking by ID.</p>
	 * 
	 * @return a reservation by ID
	 */
    @GET
    @Path("/{id:[0-9][0-9]*}")
    @Produces("application/json")
    public List<Booking> retriveBookingById(@PathParam("id") Long id) {
    	try {
    		BookingSearchParams params = new BookingSearchParams();
    		params.setId(id);
    		return service.retrieveBooking(params);
    	} catch (Exception e) {
			return null;
		}
    }
    
    /**
   	 * 
   	 * <p>Method for retrieve booking by ID.</p>
   	 * 
   	 * @return a reservation by ID
   	 */
       @GET
       @Path("/seat/{id:[0-9][0-9]*}")
       @Produces("application/json")
       public Boolean retriveBookingBySeatId(@PathParam("id") Long id) {
       	try {
       		BookingSearchParams params = new BookingSearchParams();
       		params.setSeatId(id);
       		params.setDate(currentTime.currentTimeInTimestamp());
       		if(service.retrieveBooking(params).isEmpty()) {
    			return false;
    		} else {
    			return true;
    		}
       	} catch (Exception e) {
   			return null;
   		}
       }
    
    /**
	  * <p>Method for retrieve employee by surname.</p>
	  * 
	  * @param bean {@link EmployeeBean}
	  * @throws Exception
	  */
	@POST
	@Path("/params")
	@Produces("application/json")
	public List<Booking> retrieveBookingByTimeParams(BookingSearchParams params) throws Exception {
		try {
	  			return service.retrieveBooking(params);
	    	} catch (Exception e) {
	    		return null;
			}
	}
    
//    /**
//	 * 
//	 * <p>Method for retrieve reservation by employee ID.</p>
//	 * 
//	 * @return a reservation by employee ID
//	 */
//    @GET
//    @Path("/employee/{id:[0-9][0-9]*}")
//    @Produces("application/json")
//    public List<ReservationBean> retrieveREservationByEmployeeId(@PathParam("id") Long id) {
//    	try {
//    		ReservationSearchParams params = new ReservationSearchParams();
//    		params.setReservedBy(id);
//    		return mapperToBean.toBeanList(service.retrieveReservationByEmployeeId(params));
//    	} catch (Exception e) {
//			return null;
//		}
//    }
    
    /**
     * 
     * <p>Method for delete booking by ID</p>
     * 
     * @param id
     */
    @DELETE
    @Path("/{id:[0-9][0-9]*}")
    public void deleteBookingById(@PathParam("id") Long id) {
    	try {
    		service.deleteBookingById(id);
    	} catch (Exception e) {
    	
		}
    }
    
    /**
     * 
     * <p>Method for create new booking.</p>
     * 
     * @param dto
     * @throws Exception
     */
    @POST
    @Consumes("application/json")
    public Response createNewBooking(Booking dto) throws Exception {
    	try {
    		service.createNewBooking(dto);
    	} catch (Exception e) {
    	
		}
		return Response.created(UriBuilder.fromResource(BookingEndpoint.class).path(String.valueOf(dto.getId())).build()).build();
    };
	
    /**
     * 
     * <p>Method for update reservation by their id.</p>
     * 
     * @param id
     * @param bean {@link ReservationBean}
     */
    @PUT
    @Path("/{id:[0-9][0-9]*}")
    @Consumes("application/json")
    public void updateBookingById(@PathParam("id") Long id, Booking dto) {
    	try {
    		service.updateBookingById(id, dto);
    	} catch (Exception e) {
    		
		}
    }
	
}
