package com.ibagroup.shuttleninja.database.dao;

public class SeatSearchParams {

	private Long id;
	
	private String name;
	
	private boolean isNearWindowSeat;
	
	private boolean isSingleSeat;
	
	private boolean isFrontSeat;
	
	private String bus;

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public boolean isNearWindowSeat() {
		return isNearWindowSeat;
	}

	public boolean isSingleSeat() {
		return isSingleSeat;
	}

	public boolean isFrontSeat() {
		return isFrontSeat;
	}
	
	public String getBus() {
		return bus;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNearWindowSeat(boolean isNearWindowSeat) {
		this.isNearWindowSeat = isNearWindowSeat;
	}

	public void setSingleSeat(boolean isSingleSeat) {
		this.isSingleSeat = isSingleSeat;
	}

	public void setFrontSeat(boolean isFrontSeat) {
		this.isFrontSeat = isFrontSeat;
	}
	
	public void setBus(String bus) {
		this.bus = bus;
	}
	
}
