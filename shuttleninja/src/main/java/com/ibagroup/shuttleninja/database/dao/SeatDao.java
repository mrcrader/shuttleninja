package com.ibagroup.shuttleninja.database.dao;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.ibagroup.shuttleninja.database.model.Seat;

@SuppressWarnings("serial")
@RequestScoped
public class SeatDao implements Serializable{
	
	/**
	 * <p>Injection of EntityMenager. unitName="primary" is the name of out persistence.xml</p>
	 */
	@PersistenceContext(unitName = "primary")
	private EntityManager em;
	
	@Inject
	private Event<Seat> seatEventSrc;
	
	/**
	 * <p> Return all reservations if other parameters are null </p>
	 * @param search parameters
	 * @return list of reservations
	 */
	public List<Seat> retrieveSeatByParams(SeatSearchParams params) throws NonUniqueResultException {
		if(params.getBus() != null) {
			TypedQuery<Seat> query = em.createQuery("SELECT s FROM Seat s WHERE s.id = :id OR (s.isFrontSeat = :isFrontSeat AND s.bus = :bus AND s.isNearWindowSeat = :isNearWindowSeat AND s.isSingleSeat = :isSingleSeat) OR s.name = :name", Seat.class);
				if (params.getId() != null)
					query.setParameter("id", params.getId());
				else query.setParameter("id", null);
				if (params.getBus() != null)
					query.setParameter("bus", params.getBus());
				else query.setParameter("bus", null);
				if (params.getName() != null)
					query.setParameter("name", params.getName());
				else query.setParameter("name", null);
				if (params.isFrontSeat() == true)
					query.setParameter("isFrontSeat", true);
				else query.setParameter("isFrontSeat", false);
				if (params.isNearWindowSeat() == true)
					query.setParameter("isNearWindowSeat", true);
				else query.setParameter("isNearWindowSeat", false);
				if (params.isSingleSeat() == true)
					query.setParameter("isSingleSeat", true);
				else query.setParameter("isSingleSeat", false);
				try {
					return (List<Seat>) query.getResultList();
				} catch (NoResultException exc) {
					return null;
				}
		} else {
				TypedQuery<Seat> query = em.createQuery("SELECT s FROM Seat s WHERE s.id = :id OR (s.isFrontSeat = :isFrontSeat AND s.isNearWindowSeat = :isNearWindowSeat AND s.isSingleSeat = :isSingleSeat) OR s.name = :name", Seat.class);
				if (params.getId() != null)
					query.setParameter("id", params.getId());
				else query.setParameter("id", null);
				if (params.getName() != null)
					query.setParameter("name", params.getName());
				else query.setParameter("name", null);
				if (params.isFrontSeat() == true)
					query.setParameter("isFrontSeat", true);
				else query.setParameter("isFrontSeat", false);
				if (params.isNearWindowSeat() == true)
					query.setParameter("isNearWindowSeat", true);
				else query.setParameter("isNearWindowSeat", false);
				if (params.isSingleSeat() == true)
					query.setParameter("isSingleSeat", true);
				else query.setParameter("isSingleSeat", false);
				try {
					return (List<Seat>) query.getResultList();
				} catch (NoResultException exc) {
					return null;
				}
		}

	}
	
	public List<Seat> retrieveAllSeat() throws NonUniqueResultException {
		TypedQuery<Seat> query = em.createQuery("SELECT DISTINCT s FROM Seat s ORDER BY s.id", Seat.class);
		try {
			return (List<Seat>) query.getResultList();
		} catch (NoResultException exc) {
			return null;
		}
	}
	
		public void createNewSeat(Seat dto) throws Exception {
	   		try {
		        em.persist(dto);
		        seatEventSrc.fire(dto);
	   		} catch (Exception e) {
	   	
			}
		}
	
	   	public void deleteSeatById(Long id) {
	   		Seat dto = em.find(Seat.class, id);
		      if (dto == null) {
		    	  dto = new Seat();
		      }
		      em.remove(dto);
		   }
	   
		public void updateSeatById(Long id, Seat dtoToUpdate) {
		      TypedQuery<Seat> findByIdQuery = em.createQuery("SELECT DISTINCT s FROM Seat s WHERE s.id = :dtoId ORDER BY s.id", Seat.class);
		      findByIdQuery.setParameter("dtoId", id);
		      Seat dto;
		      try {
		         dto = findByIdQuery.getSingleResult();
		      }
		      catch (NoResultException nre) {
		         dto = null;
		      }
		      dto.setBus(dtoToUpdate.getBus());
		      dto.setName(dtoToUpdate.getName());
		      dto.setFrontSeat(dtoToUpdate.isFrontSeat());
		      dto.setNearWindowSeat(dtoToUpdate.isNearWindowSeat());
		      dto.setSingleSeat(dtoToUpdate.isSingleSeat());
		      em.merge(dto);
		   }

}
