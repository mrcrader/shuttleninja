package com.ibagroup.shuttleninja.database.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@SuppressWarnings("serial")
@Entity
@XmlRootElement
@Table(name = "seats")
public class Seat implements Serializable{

	@Id
	@GeneratedValue
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "bus")
	private String bus;
	 
	@Column(name = "isSingleSeat")
	private boolean isSingleSeat;
	 
	@Column(name = "isNearWindowSeat")
	private boolean isNearWindowSeat;
	 
	@Column(name = "isFrontSeat")
	private boolean isFrontSeat;

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getBus() {
		return bus;
	}

	public boolean isSingleSeat() {
		return isSingleSeat;
	}

	public boolean isNearWindowSeat() {
		return isNearWindowSeat;
	}

	public boolean isFrontSeat() {
		return isFrontSeat;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setBus(String bus) {
		this.bus = bus;
	}

	public void setSingleSeat(boolean isSingleSeat) {
		this.isSingleSeat = isSingleSeat;
	}

	public void setNearWindowSeat(boolean isNearWindowSeat) {
		this.isNearWindowSeat = isNearWindowSeat;
	}

	public void setFrontSeat(boolean isFrontSeat) {
		this.isFrontSeat = isFrontSeat;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bus == null) ? 0 : bus.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (isFrontSeat ? 1231 : 1237);
		result = prime * result + (isNearWindowSeat ? 1231 : 1237);
		result = prime * result + (isSingleSeat ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Seat other = (Seat) obj;
		if (bus == null) {
			if (other.bus != null)
				return false;
		} else if (!bus.equals(other.bus))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isFrontSeat != other.isFrontSeat)
			return false;
		if (isNearWindowSeat != other.isNearWindowSeat)
			return false;
		if (isSingleSeat != other.isSingleSeat)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Seat [id=" + id + ", name=" + name + ", bus=" + bus + ", isSingleSeat=" + isSingleSeat
				+ ", isNearWindowSeat=" + isNearWindowSeat + ", isFrontSeat=" + isFrontSeat + "]";
	}
	
}
