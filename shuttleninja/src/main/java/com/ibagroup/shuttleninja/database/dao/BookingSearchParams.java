package com.ibagroup.shuttleninja.database.dao;

import java.sql.Timestamp;

/**
 * 
 * @author IBA Group
 * @since 2019
 *
 */

public class BookingSearchParams {

	private Long id;
	
	private String toTrip;
	
	private Timestamp date;
	
	private Long seatId;
	
	private Long userId;

	public Long getId() {
		return id;
	}

	public Timestamp getDate() {
		return date;
	}
	
	public Long getSeatId() {
		return seatId;
	}
	
	public String getToTrip() {
		return toTrip;
	}

	public Long getUserId() {
		return userId;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public void setDate(Timestamp timestamp) {
		this.date = timestamp;
	}

	public void setSeatId(Long seatId) {
		this.seatId = seatId;
	}
	
	public void setToTrip(String toTrip) {
		this.toTrip = toTrip;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
}
