package com.ibagroup.shuttleninja.database.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import javax.xml.bind.annotation.XmlRootElement;

@SuppressWarnings("serial")
@Entity
@XmlRootElement
@Table(name = "booking")
public class Booking implements Serializable{

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name = "toTrip")
	private String toTrip;
	
	private Date date;
	
	@OneToOne
	private Seat seat;
	
	@OneToOne
	private User user;

	public Long getId() {
		return id;
	}

	public String getToTrip() {
		return toTrip;
	}

	public Date getDate() {
		return date;
	}

	public Seat getSeat() {
		return seat;
	}

	public User getUser() {
		return user;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setToTrip(String toTrip) {
		this.toTrip = toTrip;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setSeat(Seat seat) {
		this.seat = seat;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((seat == null) ? 0 : seat.hashCode());
		result = prime * result + ((toTrip == null) ? 0 : toTrip.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Booking other = (Booking) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (seat == null) {
			if (other.seat != null)
				return false;
		} else if (!seat.equals(other.seat))
			return false;
		if (toTrip == null) {
			if (other.toTrip != null)
				return false;
		} else if (!toTrip.equals(other.toTrip))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Booking [id=" + id + ", toTrip=" + toTrip + ", date=" + date + ", seat=" + seat + ", user=" + user
				+ "]";
	}
	
}
