package com.ibagroup.shuttleninja.database.dao;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.ibagroup.shuttleninja.database.model.Booking;

@SuppressWarnings("serial")
@RequestScoped
public class BookingDao implements Serializable{

	/**
	 * <p>Injection of EntityMenager. unitName="primary" is the name of out persistence.xml</p>
	 */
	@PersistenceContext(unitName = "primary")
	private EntityManager em;
	
	@Inject
	private Event<Booking> bookingEventSrc;
	
	/**
	 * <p> Return all reservations if other parameters are null </p>
	 * @param search parameters
	 * @return list of reservations
	 */
	public List<Booking> retrieveBookingByParams(BookingSearchParams params) throws NonUniqueResultException {
		if (params.getId() != null || params.getDate() != null || params.getSeatId() != null) {
			TypedQuery<Booking> query = em.createQuery("SELECT b FROM Booking b WHERE b.id = :id OR (b.date >= :currentDate AND b.seat.id = :seatId)", Booking.class);
				if (params.getId() != null)
					query.setParameter("id", params.getId());
				else query.setParameter("id", null);
				if (params.getDate() != null)
					query.setParameter("currentDate", params.getDate());
				else query.setParameter("currentDate", null);
				if (params.getSeatId() != null)
					query.setParameter("seatId", params.getSeatId());
				else query.setParameter("seatId", null);
				try {
					return (List<Booking>) query.getResultList();
				} catch (NoResultException exc) {
					return null;
				}
		} else {
			TypedQuery<Booking> query = em.createQuery("SELECT DISTINCT b FROM Booking b ORDER BY b.id", Booking.class);
			try {
				return (List<Booking>) query.getResultList();
			} catch (NoResultException exc) {
				return null;
			}
		}
	}
	
		public void createNewBooking(Booking dto) throws Exception {
	   		try {
		        em.persist(dto);
		        bookingEventSrc.fire(dto);
	   		} catch (Exception e) {
	   	
			}
		}
	
	   	public void deleteBookingById(Long id) {
	   		Booking dto = em.find(Booking.class, id);
		      if (dto == null) {
		    	  dto = new Booking();
		      }
		      em.remove(dto);
		   }
	   
		public void updateBookingById(Long id, Booking dtoToUpdate) {
		      TypedQuery<Booking> findByIdQuery = em.createQuery("SELECT DISTINCT b FROM Booking b WHERE b.id = :dtoId ORDER BY b.id", Booking.class);
		      findByIdQuery.setParameter("dtoId", id);
		      Booking dto;
		      try {
		         dto = findByIdQuery.getSingleResult();
		      }
		      catch (NoResultException nre) {
		         dto = null;
		      }
		      dto.setDate(dtoToUpdate.getDate());
		      dto.setToTrip(dtoToUpdate.getToTrip());
		      em.merge(dto);
		   }
	
}
