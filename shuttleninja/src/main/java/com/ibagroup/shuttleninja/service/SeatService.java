package com.ibagroup.shuttleninja.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.ibagroup.shuttleninja.database.dao.SeatDao;
import com.ibagroup.shuttleninja.database.dao.SeatSearchParams;
import com.ibagroup.shuttleninja.database.model.Seat;

@Stateless
public class SeatService {

	@Inject
	private SeatDao dao;
	
	/*
	 * Method for invoke all booking by params
	 */
	public List<Seat> retrieveSeatByParams(SeatSearchParams params){
		try {
			return dao.retrieveSeatByParams(params);
		} catch (Exception e) {
			return null;
		}
	}
	
	public List<Seat> retrieveAllSeats(){
		try {
				return dao.retrieveAllSeat();
		} catch (Exception e) {
			return null;
		}
	}
    
    public void deleteSeatById(Long id) {
    	try {
    		dao.deleteSeatById(id);
    	} catch (Exception e) {
    		
		}
    }
    
    public void createNewSeat(Seat dto) throws Exception {
    	try {
    		dao.createNewSeat(dto);
    	} catch (Exception e) {
  
		}
    }
    
    public void updateSeatById(Long id, Seat dto) {
    	try {
    		dao.updateSeatById(id, dto);
    	} catch (Exception e) {

		}
    }
	
}
