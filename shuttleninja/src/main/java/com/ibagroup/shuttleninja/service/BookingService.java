package com.ibagroup.shuttleninja.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.ibagroup.shuttleninja.database.dao.BookingDao;
import com.ibagroup.shuttleninja.database.dao.BookingSearchParams;
import com.ibagroup.shuttleninja.database.dao.SeatDao;
import com.ibagroup.shuttleninja.database.model.Booking;

@Stateless
public class BookingService {

	@Inject
	private BookingDao dao;
	
	/*
	 * Method for invoke all booking by params
	 */
	public List<Booking> retrieveBooking(BookingSearchParams params){
		try {
			return dao.retrieveBookingByParams(params);
		} catch (Exception e) {
			return null;
		}
	}
    
    public void deleteBookingById(Long id) {
    	try {
    		dao.deleteBookingById(id);
    	} catch (Exception e) {
    		
		}
    }
    
    public void createNewBooking(Booking dto) throws Exception {
    	try {
    		dao.createNewBooking(dto);
    	} catch (Exception e) {
  
		}
    }
    
    public void updateBookingById(Long id, Booking dto) {
    	try {
    		dao.updateBookingById(id, dto);
    	} catch (Exception e) {

		}
    }
	
}
