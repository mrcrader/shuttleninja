insert into users (id, name, email, password) values (1, 'Yana', 'tyana@example.com', 'qwerty');


insert  into seats (id, name, bus, isNearWindowSeat, isFrontSeat, isSingleSeat) values (1, '1M', 'Mercedes', false, true, false);

insert  into seats (id, name, bus, isNearWindowSeat, isFrontSeat, isSingleSeat) values (2, '2M', 'Mercedes', true, true, false);

insert  into seats (id, name, bus, isNearWindowSeat, isFrontSeat, isSingleSeat) values (3, '3M', 'Mercedes', true, false, false);

insert  into seats (id, name, bus, isNearWindowSeat, isFrontSeat, isSingleSeat) values (4, '4M', 'Mercedes', false, false, false);

insert  into seats (id, name, bus, isNearWindowSeat, isFrontSeat, isSingleSeat) values (5, '5M', 'Mercedes', false, false, false);


insert into booking (id, date, toTrip, seat_id, user_id) values (1, '2019-05-16 18:40:00', 'Uruche', 1, 1);
